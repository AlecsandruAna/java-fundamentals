package exercices;

import utils.ScannerUtils;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class Task17 {
    public static void main(String[] args) {
        Task17 task17 = new Task17();
        task17.calculateDaysBetween();
    }

public void calculateDaysBetween(){
    System.out.println("Introdu data sub formatul: dd MM YYYY");
    String input = ScannerUtils.getScanner().nextLine();
    LocalDate insertedDate = LocalDate.parse(input, DateTimeFormatter.ofPattern("dd MM YYYY"));
    LocalDate currentDate = LocalDate.now();
    Period period = Period.between(currentDate, insertedDate);
    System.out.println(period.getDays());
    }
}
