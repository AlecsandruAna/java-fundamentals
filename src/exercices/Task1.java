package exercices;

import utils.ScannerUtils;

public class Task1 {
    public static void main(String[] args) {
        Task1 calcul = new Task1();
        System.out.println("Perimetrul cercului este: " + calcul.calculateCirclePer());
    }
    public double calculateCirclePer(){
        System.out.println("Introdu diametrul cercului: ");
        float diameter = ScannerUtils.getNumberFromConsole();
        return Math.PI*diameter;
    }
}
