package exercices;

import utils.ScannerUtils;

public class Task12 {
    public static void main(String[] args) {
        Task12 percentage = new Task12();
        percentage.findOccurrenceOfSpace();
    }
    public void findOccurrenceOfSpace(){
        System.out.println("Scrie un text:");
        String input = ScannerUtils.getScanner().nextLine();
        int totalLength = input.length();
        int numberOfSpaces = input.split(" ").length - 1;
        int percentage = numberOfSpaces*100/totalLength;
        System.out.println("Procentul de spatii este: " + percentage + "%");

    }
}
