package exercices;

public class Task9 {

    public static void main(String[] args) {
        Task9 waves = new Task9();
        waves.drawWaves(4, 5);
    }

    public void drawWaves(int lines, int wavesNumber){
        for(int i=1; i<=lines; i++) {
            for (int j = 1; j <= wavesNumber; j++){
                int numberOfSpacesBetweenWaves = (lines * 2) - (i * 2);
            String star = "*";
            String space = "";
            String line = "%" + i + "s" + "%" + numberOfSpacesBetweenWaves + "s" + "%-" + i + "s";
            if (numberOfSpacesBetweenWaves == 0) {
                line = "%" + i + "s" + "%s" + "%-" + i + "s";
            }
            System.out.printf(line, star, space, star);
        }
            System.out.println();
        }
    }
}
