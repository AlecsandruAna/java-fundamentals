package exercices;

import utils.ScannerUtils;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task18 {
    public static void main(String[] args) {
        Task18 task18 = new Task18();
        task18.hapciu();
    }

    public void hapciu() {
        System.out.println("Scrie ceva: ");
        String inputText = ScannerUtils.getScanner().nextLine();
        Pattern pattern = Pattern.compile("acho+!");
        Matcher matcher = pattern.matcher(inputText);//metoda care verifica daca textul respecta patternul

        System.out.println("Userul a stranutat: " + matcher.matches());



    }
}
