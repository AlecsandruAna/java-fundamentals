package exercices;

import utils.ScannerUtils;

public class Task13 {
    public static void main(String[] args) {
        Task13 obiectTest = new Task13();
        obiectTest.stutter();
    }
    public void stutter(){
        System.out.println("Scrie un text:");
        String input = ScannerUtils.getScanner().nextLine();
        String[] array = input.split(" ");
        for (String word: array) {
            System.out.print(word + " " + word + " ");

        }
    }
}
