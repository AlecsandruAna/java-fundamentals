package exercices;

import utils.ScannerUtils;

public class Task14 {
    public static void main(String[] args) {
        Task14 count = new Task14();
        count.countLettersBetween();
    }

    public void countLettersBetween(){
        System.out.println("Introdu doua litere din alfabet:");
        char firstLetter = ScannerUtils.getScanner().nextLine().charAt(0);
        char secondLetter = ScannerUtils.getScanner().nextLine().charAt(0);
        int numberOfLetters = secondLetter - firstLetter;
        System.out.println("Intre litera " + firstLetter + " si litera " + secondLetter + " sunt " + numberOfLetters + " litere." );
    }
}
