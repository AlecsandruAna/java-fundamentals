package exercices.task19;

import org.jetbrains.annotations.NotNull;

public class MainTask19 {
    public static void main(String[] args) {
        Poem luceafarul = new Poem();
        luceafarul.setStropheNumbers(96);
        luceafarul.setCreator(new Author("Mihai", "romana"));

        Poem plumb = new Poem(new Author("George", "roamana"), 5);

        Author mihai = new Author("Mihai", "romana");
        Poem somnoroasePasarele = new Poem(mihai, 16);

        Poem[] poemArray = new Poem[]{luceafarul, plumb, somnoroasePasarele};
        System.out.println("Numele autorului care a scris cel mai lung poem este: " + findLongestPoem(poemArray).getCreator().getSurname());
    }
    public static Poem findLongestPoem(Poem @NotNull [] poems){
        Poem longestPoem = poems[0];
        for (Poem poem : poems){
            if (longestPoem.getStropheNumbers() < poem.getStropheNumbers()){
                longestPoem = poem;
            }
        }
        return longestPoem;
    }
}
