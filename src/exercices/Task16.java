package exercices;

import utils.ScannerUtils;

public class Task16 {

    public static void main(String[] args) {
        Task16 test = new Task16();
        System.out.println("Cel mai lung sir de numere este " + test.longestConsecutiveNumbers());
    }

    public int longestConsecutiveNumbers() {
        int[] inputNumbers = new int[10];
        System.out.println(" Introduceti 10 numere: ");
        for (int i = 0; i < inputNumbers.length; i++) {
            inputNumbers[i] = ScannerUtils.getNumberFromInput();
        }
        int consecutiveNumbers = 1;
        int longestNumbers = 1;
        for (int i = 0; i < inputNumbers.length; i++) {
            if (inputNumbers[i] < inputNumbers[i + 1]) {
                consecutiveNumbers++;
                if (longestNumbers < consecutiveNumbers) {
                    longestNumbers = consecutiveNumbers;
                }
            } else {
                consecutiveNumbers = 1;
            }
            if (i == inputNumbers.length - 2)
                break;
        }
        return longestNumbers;
    }

}
