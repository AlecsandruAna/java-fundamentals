package exercices;

import utils.ScannerUtils;

public class Task15 {
    public static void main(String[] args) {
        Task15 task = new Task15();
        task.printNumbersThatOccurredTwice();
    }
    public void printNumbersThatOccurredTwice(){
        System.out.println("Introdu 10 numere:");
        int[] numbersFromUser = new int[10];
        for (int i=0; i<numbersFromUser.length; i++){
            numbersFromUser[i]= ScannerUtils.getNumberFromInput();
        }

        for (int i=1; i<numbersFromUser.length; i++){
            if (numbersFromUser[i] == -1){
                continue;
            }
            int number = numbersFromUser[i];
            int duplicateNumber = -1;
            for (int j=i+1; j<numbersFromUser.length; j++){
                if (number == numbersFromUser[j]){
                    duplicateNumber = number;
                    numbersFromUser[j] = -1;
                }
            }
            if (duplicateNumber != -1) {
                System.out.println(number);
            }
        }

    }
}
