import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //System.out.printf("Numele meu este %s si prenumele este %s","Matei", "Madalina");//cu %s formatam elem
//        printName();
//        printTable();
//        printTableWithStringFormat();
//        printFormattedNumber();
//        triangle(10);
        printNumberTriangle(readNumberFromConsole());

    }
    public static void printName(){
        Scanner scan = new Scanner(System.in);
        System.out.println("Inrodu numele de familie: ");
        String name = scan.nextLine();
        System.out.println("Introdu prenumele: ");
        String surname = scan.nextLine();
        System.out.printf("Numele de familie %s si prenumele este %s", surname, name);// %10s adauga spatii inainte de cuv

    }
    public static void printTable(){
        System.out.printf("%s %21s\n", "Exam name", "Exam grade");
        System.out.println("-----------------------------------");
        System.out.printf("%-20s %s\n", "Java", "A");
        System.out.printf("%-20s %s\n", "Pro", "B");
        System.out.printf("%-20s %s\n", "PHP", "C");
        System.out.println("-----------------------------------");

    }
    private static void printTableWithStringFormat(){
        System.out.println(String.format("%-20s %s", "Exam Name", "Exam Grade"));
        System.out.println("-------------------------------------------");
        System.out.println(String.format("%-24s %s", "Java", "A"));
        System.out.println(String.format("%-24s %s", "Php", "B"));
        System.out.println(String.format("%-24s %s", "VB net", "A"));
        System.out.println("-------------------------------------------");
    }
    public static void printFormattedNumber(){
        System.out.printf("Numarul %2f este formatat ", 6.5);
    }
    public static void triangle(int n){
        for (int i=1; i<=n; i++){
            for (int j=1; j<=i; j++){
                System.out.print(j + " ");
            }

            System.out.println();

        }
    }
    public static int readNumberFromConsole(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introdu numar: ");
        return scanner.nextInt();
    }
    public static void printNumberTriangle(int number){
        String text = "";
        for (int i=1; i<=number; i++){
            text = text + i + " ";
            System.out.println(text);
        }
    }

}
