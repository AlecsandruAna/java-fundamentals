package utils;

import java.util.Scanner;

public class ScannerUtils {
    private static Scanner scanner;

    public static int getNumberFromInput() {
        Scanner scanner = getScanner();
        int userNumberInput;
        try {
            userNumberInput = scanner.nextInt();
        } catch (Exception exception) {
            System.out.println("Introdu un numar valid");
            scanner.nextLine();
            userNumberInput = getNumberFromInput();
        }
        return userNumberInput;
    }

    public static Scanner getScanner() {
        if (scanner == null) {
            scanner = new Scanner(System.in);
        }
        return scanner;
    }

    public static float getNumberFromConsole() {
        Scanner scanner = getScanner();
        float userNumberInput;
        try {
            userNumberInput = scanner.nextFloat();
        } catch (Exception exception) {
            System.out.println("Introdu un numar valid");
            scanner.nextLine();
            userNumberInput = getNumberFromInput();
        }
        return userNumberInput;
    }
}
